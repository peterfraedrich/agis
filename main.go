package main

import (
	"fmt"

	"github.com/jinzhu/configor"
)

var config Config
var dropins []string

func main() {
	configor.Load(&config, "config.yaml")
	dropins, err := loadDropIns(config.Handlers.Dir, config.Handlers.Ignore)
	if err != nil {
		panic(err)
	}
	for _, r := range config.Routes {
		if !contains(dropins, r.Handler) {
			panic(fmt.Errorf("Dropin folder does not contain script" + r.Handler))
		}
	}
	api := generateAPI(config)
	api.Run(config.Server.Host + ":" + config.Server.Port)
}
