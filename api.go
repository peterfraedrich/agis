package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-cmd/cmd"
)

func generateAPI(config Config) *gin.Engine {
	api := gin.Default()

	for _, route := range config.Routes {
		m := strings.ToUpper(route.Method)
		switch m {
		case "GET":
			api.GET(route.Path, routeHandler)
		case "PUT":
			api.PUT(route.Path, routeHandler)
		case "POST":
			api.POST(route.Path, routeHandler)
		case "OPTIONS":
			api.OPTIONS(route.Path, routeHandler)
		case "PATCH":
			api.PATCH(route.Path, routeHandler)
		case "DELETE":
			api.DELETE(route.Path, routeHandler)
		case "HEAD":
			api.HEAD(route.Path, routeHandler)
		default:
			continue
		}
	}
	return api
}

func routeHandler(c *gin.Context) {
	handler := matchPathToHandler(c.Request.URL.String())
	if !handlerExists(handler) {
		c.Status(404)
	}
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.String(500, err.Error())
	}
	gocmd := cmd.NewCmd(handler, string(body[:]))
	done := gocmd.Start()
	res := <-done
	out := strings.Join(res.Stdout, ",")
	c.String(mapRC(res.Exit), out)
}

func matchPathToHandler(path string) string {
	for _, route := range config.Routes {
		if path == route.Path {
			return fmt.Sprintf("%s/%s", config.Handlers.Dir, route.Handler)
		}
	}
	return ""
}

func handlerExists(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
