package main

//Config type
type Config struct {
	Server struct {
		Port string `default:"5000" env:"port"`
		Host string `default:"127.0.0.1" env:"host"`
	}
	Handlers struct {
		Dir    string   `default:"handlers.d" env:"handlers"`
		Ignore []string `default:""`
	}
	Routes []Route
}

//Route type
type Route struct {
	Path    string
	Handler string
	Method  string `default:"GET"`
}

//HandlerInput type
type HandlerInput struct {
	QueryParams map[string][]string
	Body        []byte
}

//HandlerOutput type
type HandlerOutput struct {
	rc         int
	output     interface{}
	outputtype string
}

var returnCodes = map[int]int{
	0:  200,
	1:  201,
	2:  202,
	3:  204,
	4:  400,
	5:  401,
	6:  403,
	7:  404,
	8:  408,
	9:  409,
	15: 418,
	10: 429,
	14: 500,
	11: 501,
	12: 502,
	13: 503,
}
