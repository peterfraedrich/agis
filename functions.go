package main

import (
	"io/ioutil"
	"strings"
)

func contains(list []string, item string) bool {
	for _, i := range list {
		if i == item {
			return true
		}
	}
	return false
}

func ignore(list []string, item string) bool {
	for _, i := range list {
		if strings.HasSuffix(item, i) {
			return true
		}
	}
	return false
}

func loadDropIns(path string, ignoreArray []string) ([]string, error) {
	dropins := []string{}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return dropins, err
	}
	for _, f := range files {
		if ignore(ignoreArray, f.Name()) {
			continue
		}
		dropins = append(dropins, f.Name())
	}
	return dropins, nil
}

func mapRC(rc int) int {
	if len(returnCodes) < rc {
		return 500
	}
	return returnCodes[rc]
}
