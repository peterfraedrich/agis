# agis

Advanced Gateway Interface Server (AGIS) is a simple HTTP server that takes a config file (yaml) of routes, methods, and handlers, and serves them up.

## Config
the config file looks something like this:

```yaml
server:
  # server port
  port: 5000
  # server host
  host: 127.0.0.1

handlers:
  # directory to find the handlers, this will be appended to all `handler:` script names in the route definitions
  dir: handlers.d
  # file name extensions/suffixes to ignore (ex, py, rb)
  ignore:
    - none

routes:
  # a route definition
    # path: the path for the server to respond to. this translates to http://127.0.0.1:5000/ping
  - path: "/ping"
    # handler script to invoke when a request is handled
    handler: ping.sh
    # the HTTP method
    method: GET

    # routes can have different handlers for different methods at the same path
  - path: "/hello"
    handler: test.py
    method: POST
  
  - path: "/hello"
    handler: hello.sh
    method: GET
```

## POST/PUT
the request body for `POST` and `PUT` requests will be passed to the handler has the first argument (`$1` for bash) in the same format that the body is sent in. this means that headers are currently ignored, so your handlers will have to inspect to see if the data is the proper format or not.

## Exit Codes
handler exit codes are translated to HTTP status codes based on the following table:
| handler exit | http status code | explaination          |
|--------------|------------------|-----------------------|
|0             | 200              | success
|1             | 201              | created
|2             | 202              | accepted
|3             | 204              | no content
|4             | 400              | bad request
|5             | 401              | unauthorized
|6             | 403              | forbidden
|7             | 404              | not found
|8             | 408              | request timeout
|9             | 409              | conflict
|15            | 418              | im a teapot
|10            | 429              | too many requests
|14            | 500              | internal server error
|11            | 501              | not implemented
|12            | 502              | bad gateway
|13            | 503              | service unavailable

[more info](https://httpstatuses.com/)

### Notes/TODO:
* handlers must be executable and have a `hashbang`
* any output from a handler will be passed back to the requestor
* multi-line output will be joined with commas
* HTTPS/SSL is not implemented (yet)
* headers and query strings are not inspected or passed to handlers (yet)
